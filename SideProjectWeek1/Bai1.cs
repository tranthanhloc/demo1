﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideProjectWeek1
{
    public class Bai1
    {
        public void InputArrBai1(out int [] arr,ref int n)
        {
            arr = new int[n];
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("B1_Arr[{0}] = ",i);
                arr[i] = int.Parse(Console.ReadLine());
            }
        }

        public long CalcNumSum(int [] arr)
        {
            int Count = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr.Length; j++)
                {
                    if (arr[j] < arr[i])
                    {
                        for (int k = 0; k < arr.Length; k++)
                        {
                            if (arr[k] < arr[i] && arr[k] != arr[j] && arr[j] + arr[k] == arr[i])
                            {
                                Count++;
                            }
                        }
                    }
                }
            }

            return Count/2;
        }
    }
}
