﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideProjectWeek1
{
    public class Bai3
    {
        public bool PalindromeString(string s)
        {
            bool x = false;

            string reverse = string.Empty;
            for (int i = s.Length - 1; i >= 0; i--)
                reverse += s[i];

            if (string.Compare(s, reverse, false) == 0)
                x = true;
            return x;
        }
    }
}
