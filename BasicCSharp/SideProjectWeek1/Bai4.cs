﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideProjectWeek1
{
    public class Bai4
    {
        public bool CheckPasswordPolicy(string password)
        {
            bool check = false;
            int sum = 0;
            if (password.Length >= 6)
                sum++;

            for (int i = 0; i < password.Length; i++)
            {
                if (char.IsUpper(password[i]))
                {
                    sum++;
                    break;
                }
            }

            for (int i = 0; i < password.Length; i++)
            {
                if (char.IsLower(password[i]))
                {
                    sum++;
                    break;
                }
            }

            for (int i = 0; i < password.Length; i++)
            {
                if (char.IsNumber(password[i]))
                {
                    sum++;
                    break;
                }
            }

            if (sum == 4)
                check = true;
            return check;
        }
    }
}
