﻿using System;

namespace SideProjectWeek1
{
    public class Program
    {
        static public void Main(string[] args)
        {
            do
            {
                Console.Clear();
                Console.WriteLine("1. Run Bai1");
                Console.WriteLine("2. Run Bai2");
                Console.WriteLine("3. Run Bai3");
                Console.WriteLine("4. Run Bai4");
                Console.WriteLine("7. Thoat");
                string choose = Console.ReadLine();
                switch (choose)
                {
                    case "1":
                        RunBai1();
                        Console.ReadLine();
                        break;
                    case "2":
                        RunBai2();
                        Console.ReadLine();
                        break;
                    case "3":
                        RunBai3();
                        Console.ReadLine();
                        break;
                    case "4":
                        RunBai4();
                        Console.ReadLine();
                        break;
                    case "7":
                        return;
                    default:
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.WriteLine("\nEnter Repeat:");
                        Console.ReadLine();
                        break;
                }
            } while (true);

        }
        static public void RunBai1()
        {
            int nb1;
            Console.WriteLine("Input n:");
            nb1 = int.Parse(Console.ReadLine());
            int[] arrb1;

            Bai1 enforce1 = new Bai1();
            enforce1.InputArrBai1(out arrb1, ref nb1);

            Console.WriteLine(enforce1.CalcNumSum(arrb1));
        }

        static public void RunBai2()
        {
            int nb2;
            Console.WriteLine("Input nb2: ");
            nb2 = int.Parse(Console.ReadLine());

            int mb2;
            Console.WriteLine("Input mb2: ");
            mb2 = int.Parse(Console.ReadLine());

            int[,] arrb2;

            Bai2 enforce2 = new Bai2();
            enforce2.InputArrBai2(out arrb2, ref nb2, ref mb2);
            Console.WriteLine(enforce2.SumPrimeInMatrix(arrb2));
        }

        static public void RunBai3()
        {
            Bai3 enforce3 = new Bai3();
            Console.WriteLine("Enter String: ");
            string s = Console.ReadLine();

            Console.WriteLine(enforce3.PalindromeString(s));
        }
        static public void RunBai4()
        {
            Bai4 enforce4 = new Bai4();
            Console.WriteLine("Enter Password: ");
            string password = Console.ReadLine();

            Console.WriteLine(enforce4.CheckPasswordPolicy(password));
        }
        
    }
}