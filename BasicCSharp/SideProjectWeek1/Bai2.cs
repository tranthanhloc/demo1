﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideProjectWeek1
{
    public class Bai2
    {
        public void InputArrBai2(out int[,] arr,ref int n,ref int m)
        {
            arr = new int[n, m];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                {
                    Console.Write("arr[{0},{1}] = ", i, j);
                    arr[i, j] = int.Parse(Console.ReadLine());
                }
        }

        public int SumPrimeInMatrix(int [,] arr)
        {
            int Sum = 0;
            int Count = 0;
            for (int i = 0; i < arr.GetLength(0); i++)
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if(arr[i,j] >= 2)
                    {
                        for(int k = 2; k <= Math.Sqrt(arr[i,j]); k++)
                            if (arr[i, j] % k == 0)
                                Count++;
                        if (Count == 0)
                            Sum += arr[i, j];
                    }
                }
            return Sum;
        }
    }
}
